﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCows : MonoBehaviour
{

    public Transform[] spawnPoint;
    public GameObject[] cow;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawning());
    }

    IEnumerator StartSpawning(){
        yield return new WaitForSeconds(3);

        float screen_height = Screen.height/300;
        float y = Random.Range(-screen_height, screen_height);

        float z = 0.1f;

        float x = Screen.width/300;
        if(Random.Range(0.0f, 1.0f) > 0.5){
            x = -x;
            Instantiate(cow[0], spawnPoint[0].position + new Vector3(x, y, z), spawnPoint[0].rotation);
        }
        else{
            Instantiate(cow[0], spawnPoint[0].position + new Vector3(x, y, z), spawnPoint[1].rotation);
        }


        StartCoroutine(StartSpawning());
    }
}

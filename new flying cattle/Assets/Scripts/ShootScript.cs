﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class ShootScript : MonoBehaviour 
{
    public GameObject arCamera;
    public GameObject smoke;
    public Animator m_Animator;

    public GameObject sText;
    public int theScore;

    public void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
        {
            if (hit.transform.name == "Cow(Clone)")
            {
                m_Animator = hit.transform.gameObject.GetComponent<Animator>();
                m_Animator.SetBool("Shot", true);
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));

                theScore += 8900;
                sText.GetComponent<Text>().text = "SCORE: " + theScore;
            }
        }
    }

    public IEnumerator Waiting()
    {
        yield return new WaitForSeconds(3);
    }
}